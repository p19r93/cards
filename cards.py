# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 12:59:38 2018

@author: Peter.Rhodes
"""

import reprlib
from copy import deepcopy


class Suit:
    def __init__(self, name, symbol, colour, precedence=0):
        self.name = name
        self.symbol = symbol
        self.colour = colour
        self.precedence = precedence
        
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return f"Suit. name: {self.name}, symbol: {self.symbol}, colour: {self.colour}"
    
    def __gt__(self, other):
        return self.precedence > other.precedence
    
    def __eq__(self, other):
        return self.precedence == other.precedence
    
    def __ge__(self, other):
        return self > other or self == other


class Rank:
    def __init__(self, label, hierarchy=None, value=None):
        if value is None:
            value = int(label)
        if hierarchy is None:
            hierarchy = value
        self.label = label
        self.value = value
        self.hierarchy = hierarchy
        
    def __str__(self):
        return self.label
    
    def __add__(self, other):
        return self.value + other.value
    
    def __gt__(self, other):
        return self.hierarchy > other.hierarchy
    
    def __eq__(self, other):
        return self.hierarchy == other.hierarchy
    
    def __ge__(self, other):
        return (self > other or self == other)


class Card:   
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit
        
    def __str__(self):
        return f"{str(self.rank):>2}{self.suit.symbol}"
    
    def __repr__(self):
        return self.__str__() + "   " + super().__repr__()
    
    def __gt__(self, other):
        return self.hierarchy > other.hierarchy
        
    def __eq__(self, other):
        return (self.rank == other.rank)
    
    def __ge__(self, other):
        return (self > other or self == other)
    
    def __add__(self, other):
        return self.value + other.value
    
    @property
    def colour(self):
        return self.suit.colour
    
    @property
    def hierarchy(self):
        return self.rank.hierarchy
    
    @property
    def value(self):
        return self.rank.value
    

class Deck:
    def __init__(self, suits=None, ranks=None):
        if suits is not None:
            assert ranks is not None
            self._cards = [Card(rank, suit) for suit in suits
                                        for rank in ranks]
        else:
            self._cards = []
        
    def __len__(self):
        return len(self._cards)
    
    def __getitem__(self, position):
        return self._cards[position]
    
    def __setitem__(self, position, card):
        self._cards[position] = card
    
    def __str__(self):
        return f"Deck of {len(self)} cards"
    
    def __repr__(self):
        return super().__repr__() + ' -- ' + self.__str__() + ":" \
            + reprlib.repr(' '.join([str(card) for card in self._cards]))
    
    def __add__(self, other):
        new = deepcopy(self)
        new._cards += other._cards
        return new
    
    def __radd__(self, other):
        return self + other
    
    def __iadd__(self, other):
        if isinstance(other, Card):
            raise TypeError("To add a card to the deck, use the 'add' function")
        self._cards += other._cards
        return self
    
    def __mul__(self, num):
        assert num >= 1
        one_deck = deepcopy(self)
        new_deck = deepcopy(self)
        for i in range(num-1):
            new_deck += one_deck
        return new_deck
    
    def __imul__(self, num):
        assert num >= 1
        one_deck = deepcopy(self)
        for i in range(num-1):
            self += one_deck
        return self
    
    def draw(self, index=-1):
        """ Draws from the back (face-down) side of the pack """
        return self._cards.pop(index)
    
    def add(self, cards, face='down'):
        if isinstance(cards, Card):
            cards = [cards]
        if face == 'down':
            self._cards += cards
        elif face == 'up':
            self._cards = cards + self._cards
        else:
            raise ValueError("face must be one of 'down' or 'up'")
        

class FrenchDeck(Deck):
    
    ranks = [Rank(str(n)) for n in range(2, 11)] \
          + [Rank('J', 11, 10)] \
          + [Rank('Q', 12, 10)] \
          + [Rank('K', 13, 10)] \
          + [Rank('A', 14, 11)]
          
    suits = [
            Suit('spades', '♠', 'black'),
            Suit('diamonds', '♦', 'red'),
            Suit('clubs', '♣', 'black'),
            Suit('hearts', '♥', 'red'),
            ]
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, suits=FrenchDeck.suits, ranks=FrenchDeck.ranks, **kwargs)
