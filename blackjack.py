from random import shuffle

from cards import FrenchDeck

yes_words = ['yes', 'y', 'ok']
hit_words = ['hit', 'hit me', 'another']
no_words = ['no', 'n']
stand_words = ['stand', 'stick']
quit_words = ['q', 'quit', 'exit']


class BlackJackPlayer:
    def __init__(self, name='Player'):
        assert len(name) <= 20
        self.name = name
        self.cards = []

    def __str__(self):
        return self.name

    def bust(self):
        return self.hand_total > 21

    def deal_card(self, card):
        self.cards.append(card)

    @property
    def hand_totals(self):
        """ Return all possible totals of hand """
        tot = sum(card.value for card in self.cards)
        tots = [tot]
        for card in self.cards:
            if card.rank.label == 'A':
                tot -= 10
                tots.append(tot)
        return tots

    @property
    def hand_total(self):
        return min(self.hand_totals)

    @property
    def best_total(self):
        return max(total for total in self.hand_totals if total <= 21)

    @property
    def hand(self):
        return ', '.join([str(card) for card in self.cards])

    def deal(self):
        while True:
            if self.hand_total == 21:
                return False
            inp = input(f"Another card? (current hand {self.hand}, min total {self.hand_total})    ")
            if inp.lower() in hit_words + yes_words:
                return True
            elif inp.lower() in no_words + stand_words:
                return False
            elif inp.lower() in quit_words:
                quit()


class BlackJackDealer(BlackJackPlayer):
    def __init__(self, *args, name='Dealer', **kwargs):
        self.hit_on_soft_17 = kwargs.pop('hit_on_soft_17', True)  # Default True
        super().__init__(*args, name=name, **kwargs)

    def deal(self):
        # Check for soft 17
        if self.hit_on_soft_17 and self.hand_total == 17:
            for card in self.cards:
                if card.rank.label == 'A':
                    return True
        return self.best_total < 17


def blackjack(num_players=1, num_decks=6, charlie=False):
    deck = FrenchDeck()*num_decks
    shuffle(deck)
    dealer = BlackJackDealer()
    players = [dealer] + [BlackJackPlayer(name='Player '+str(i+1)) for i in range(num_players)]
    cards_dealt = 0
    for player in players:
        print(f"\n---{str(player):^18}---")

        # Check if everyone is bust already, and skip if so
        num_players = len(players)
        num_bust_players = len([bust_player for bust_player in players if bust_player.bust()])
        if not player.bust() and num_bust_players == num_players - 1:
            print("All other players already bust!")
            continue

        player.deal_card(deck[cards_dealt])
        cards_dealt += 1
        player.deal_card(deck[cards_dealt])
        cards_dealt += 1
        print(f"Hand: {player.hand}")
        if player.best_total == 21:
            print("Blackjack!")
            continue
        while player.deal():
            card = deck[cards_dealt]
            cards_dealt += 1
            print(f"{player} dealt {card}")
            player.deal_card(card)
            if player.bust():
                print(f"{player} bust!")
                break
            elif charlie and len(player.cards) >= charlie:
                print(f"{player} wins with {charlie}-card charlie!")
                quit()
        else:
            print(f"{player} stands with total {player.best_total} from hand {player.hand}")

    non_bust_players = [player for player in players if not player.bust()]

    # Quick newline to make space for the results
    print()

    if not non_bust_players:
        print("Everyone is bust!")
    elif len(non_bust_players) == 1:
        plyr = non_bust_players[0]
        print(f"Last player standing is {plyr} with total" 
              f" {plyr.best_total} from hand {plyr.hand}")
    else:
        winners = [non_bust_players[0]]
        best = non_bust_players[0].best_total

        for player in non_bust_players[1:]:
            if player.best_total > best:
                best = player.best_total
                winners = [player]
            elif player.best_total == best:
                winners.append(player)

        if len(winners) == 1:  # One clear winner on points
            print(f"Winner is {winners[0].name} with total"
                  f" {winners[0].best_total} from hand {winners[0].hand}")

        else:  # It's a tie
            # Check for blackjack
            winners_with_blackjack = []
            for winner in winners:
                if len(winner.cards) == 2 and best == 21:
                    winners_with_blackjack.append(winner)
            if len(winners_with_blackjack) == 1:
                plyr = winners_with_blackjack[0]
                print(f"{plyr} wins with blackjack ({plyr.hand})!")
            else:
                winners_list_string = ', '.join(str(winner) for winner in winners[:-1]) + " and " + str(winners[-1])
                print(f"Tie between {winners_list_string}, each with total {best}")


if __name__ == "__main__":
    blackjack()
